import java.util.ArrayList;
import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
   }


   /*Olgu massiivis juhuslikult läbisegi punased,  rohelised ja sinised
        pallid. Koostage võimalikult kiire meetod, mis järjestaks
        massiivi ümber nii, et kõik punased pallid oleksid massiivi
        alguses ja  kõik  sinised   pallid lõpus (rohelised vastavalt keskel).
        Arvestage ka piirjuhtumiga, et kõik pallid on üht värvi

        punased, rohelised, sinised
   */
   public static void reorder (Color[] balls) {

       /*
       Working and fast solution based on counting distinct occurences of each Color value
        */
       reorder_counts(balls);

       /*
       NOT WORKING test solution to not count but build an array of each color and then add those (max 3) arrays together.
       Less loops but a lot of conversions which failed in the end
        */
       //reorder_arrays(balls);

   }

    public static void reorder_counts (Color[] balls) {

       int reds =0;
       int greens =0;

        for (int i = 0; i < balls.length; i++) {
            switch (balls[i]) {
               case red   : reds++; break;
               case green : greens++; break;
               //case blue    : blues++; break; //not needed to count blues, number of blues is total minus reds minus greens
            }
        }

       for (int i = 0; i < balls.length; i++) {
            if(i<reds){
                balls[i] = Color.red ;
            } else if (i<reds+greens){
                balls[i] = Color.green ;
            }else {
                balls[i] = Color.blue ;
            }
       }
   }

    public static void reorder_arrays (Color[] balls) {
        //Color[] balls_temp = new Color[balls.length];

        ArrayList<Color> reds = new ArrayList<Color>();
        ArrayList<Color> greens = new ArrayList<Color>();
        ArrayList<Color> blues = new ArrayList<Color>();

        for (int i = 0; i < balls.length; i++) {
            switch (balls[i]) {
                case red   : reds.add(Color.red); break;
                case green : greens.add(Color.green); break;
                case blue    : blues.add(Color.blue); break;
            }
        }

        if (reds.size()!=0){
            System.arraycopy(reds.toArray(balls), 0, balls, 0, reds.size());
        }
        if (greens.size()!=0){
            System.arraycopy(greens.toArray(balls), 0, balls, reds.size(), greens.size());
        }
        if(blues.size()!=0){
            System.arraycopy(blues.toArray(balls), 0, balls, reds.size()+greens.size(), blues.size());
        }
    }

}

